#SPRIG Archive
The Student Pharmacist Research Interest Group (SPRIG) is dedicated to encouraging and promoting involvement in basic/clinical research among pharmacy students at the
University of New Mexico College of Pharmacy. This repository contains archived material since the organization began.

### By-laws and Constitution
1. [SPRIG By-Laws.pdf](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/bylaws/SPRIG%20By-Laws.pdf?at=master)
2. [SPRIG Proposal](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/bylaws/Student%20Pharmacist%20Research%20Interest%20Group%20Proposal.pdf?at=master)


### Interviews
1. [2014-10-14-Mercier-Interview.pdf](https://bytebucket.org/sprig/archive/raw/b7c318c46e27a09fcb57c4b281bcbd968e071f19/interviews/2014-10-14-Mercier-Interview.pdf?token=47e9c6bfe8261e9b41a5ddf2238e74d8d13e1236)

### Journal Club
1. [Consort 2010 Checklist](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/journal_club/3-26-2013%20Consort%202010%20Checklist%20of%20Information%20to%20Include%20When%20Reporting%20Randomized%20Trial.pdf?at=master)
2. [Duodenal Infusion of Donor Feces for Recurrent C. Difficile](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/journal_club/3-26-2013%20Duodenal%20Infusion%20of%20Donor%20Feces%20for%20Recurrent%20C%20Diff.pdf?at=master)

### Minutes
1. [2012-10-03-SPRIG-Meeting.pdf](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/minutes/2012-10-03%20SPRIG%20Meeting.pdf?at=master)
2. [2012-11-06 SPRIG Minutes.pdf](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/minutes/2012-11-06%20SPRIG%20Minutes.pdf?at=master)
3. [2012-12-06 SPRIG Minutes.pdf](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/minutes/2012-12-06%20SPRIG%20Minutes.pdf?at=master)
4. [2013-01-30 SPRIG Minutes.pdf](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/minutes/2013-01-30%20SPRIG%20Minutes.pdf?at=master)
5. [2013-02-21 SPRIG Minutes.pdf](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/minutes/2013-02-21%20SPRIG%20Minutes.pdf?at=master)
6. [2013-03-26 SPRIG Minutes.pdf](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/minutes/2013-03-26%20SPRIG%20Minutes.pdf?at=master)


### Newsletters
1. [SPRIG Welcome Letter](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/newsletters/2013_SPRIG_welcome_letter.pdf?at=master)
2. [LoboRx Newsletter 2014](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/newsletters/sprig_loborx_newsletter_spring_2014.pdf?at=master)


### Photos
1. [2013 SPRIG VA CSP Tour Photos](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/photos/2013%20SPRIG%20VA%20CSP%20Tour%20Photos/?at=master)
2. [2014 COP Research Day](https://bitbucket.org/sprig/archive/src/b7c318c46e27a09fcb57c4b281bcbd968e071f19/photos/2014%20COP%20Research%20Day/?at=master)

### Presentations

1. [2012-12-06-SPRIG-Post-Graduate-Training.pdf](https://bytebucket.org/sprig/archive/raw/20ad4b4599ad5dac4e31c77e5bf41a510c5981ff/presentations/2012-12-06-SPRIG-Post-Graduate-Training.pdf?token=c24447d6578bf29d8b9b587d6ff0d88deccd9959)
2. [2013-04-01-Advanced-PubMed.pdf](https://bitbucket.org/sprig/archive/src/20ad4b4599ad5dac4e31c77e5bf41a510c5981ff/presentations/2013-04-01-Advanced-PubMed.pdf?at=master)
3. [2013-04-01-Improving-Literature-Skills.pdf](https://bitbucket.org/sprig/archive/src/20ad4b4599ad5dac4e31c77e5bf41a510c5981ff/presentations/2013-04-01-Improving-Literature-Skills.pdf?at=master)
4. [2013-08-16-SPRIG-Orientation.pdf](https://bitbucket.org/sprig/archive/src/20ad4b4599ad5dac4e31c77e5bf41a510c5981ff/presentations/2013-08-16-SPRIG-Orientation.pdf?at=master)
5. [2013-08-27-Getting-Involved.pdf](https://bitbucket.org/sprig/archive/src/20ad4b4599ad5dac4e31c77e5bf41a510c5981ff/presentations/2013-08-27-Getting-Involved.pdf?at=master)
6. [2013-10-22-Journal-Club.pdf](https://bitbucket.org/sprig/archive/src/20ad4b4599ad5dac4e31c77e5bf41a510c5981ff/presentations/2013-10-22-Journal-Club.pdf?at=master)
7. [2014-08-28-Getting-Involved.pdf](https://bitbucket.org/sprig/archive/src/20ad4b4599ad5dac4e31c77e5bf41a510c5981ff/presentations/2014-08-28-Getting-Involved.pdf?at=master)
8. [2014-12-04-SPRIG-Deans-SRF-Meeting.pdf](https://bitbucket.org/sprig/archive/src/20ad4b4599ad5dac4e31c77e5bf41a510c5981ff/presentations/2014-12-04-SPRIG-Deans-SRF-Meeting.pdf?at=master)

### Reference Material
